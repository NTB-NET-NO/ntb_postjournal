Imports System.Text.RegularExpressions
Imports comUthevSok
Imports ntb_FuncLib
Imports System.Web.Mail

Public Class Form1
    Inherits System.Windows.Forms.Form

    Dim meldFrom As String = "ntb@ntb.no"
    Dim mailSubject As String = "S�k i Elektronisk Postjournal"
    Dim smtpServer As String = "notabene.ntb.no"

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBoxTestFrom As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxTestTo As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxSok As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents RadioButtonDir As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButtonDep As System.Windows.Forms.RadioButton
    Friend WithEvents TextBoxDato As System.Windows.Forms.TextBox

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.TextBox4 = New System.Windows.Forms.TextBox
        Me.TextBox3 = New System.Windows.Forms.TextBox
        Me.Button4 = New System.Windows.Forms.Button
        Me.TextBoxTestTo = New System.Windows.Forms.TextBox
        Me.TextBoxTestFrom = New System.Windows.Forms.TextBox
        Me.TextBoxSok = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.RadioButtonDir = New System.Windows.Forms.RadioButton
        Me.RadioButtonDep = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBoxDato = New System.Windows.Forms.TextBox
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(16, 184)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(152, 24)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "S�k i Journal"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(248, 8)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(552, 512)
        Me.TabControl1.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.TextBox1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(544, 486)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Renset HTML"
        '
        'TextBox1
        '
        Me.TextBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox1.Location = New System.Drawing.Point(0, 0)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox1.Size = New System.Drawing.Size(544, 486)
        Me.TextBox1.TabIndex = 2
        Me.TextBox1.Text = ""
        Me.TextBox1.WordWrap = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TextBox2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(544, 486)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Raw HTML"
        '
        'TextBox2
        '
        Me.TextBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TextBox2.Location = New System.Drawing.Point(0, 0)
        Me.TextBox2.Multiline = True
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBox2.Size = New System.Drawing.Size(544, 486)
        Me.TextBox2.TabIndex = 3
        Me.TextBox2.Text = ""
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.TextBox4)
        Me.TabPage3.Controls.Add(Me.TextBox3)
        Me.TabPage3.Controls.Add(Me.Button4)
        Me.TabPage3.Controls.Add(Me.TextBoxTestTo)
        Me.TabPage3.Controls.Add(Me.TextBoxTestFrom)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(544, 486)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Test uthev s�k"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(240, 32)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(208, 20)
        Me.TextBox4.TabIndex = 8
        Me.TextBox4.Text = "<hit>$&</hit>"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(240, 8)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(288, 20)
        Me.TextBox3.TabIndex = 7
        Me.TextBox3.Text = "\b(test|er)(ing|ene)?\b"
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(16, 8)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(104, 24)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "Test RegEx"
        '
        'TextBoxTestTo
        '
        Me.TextBoxTestTo.Location = New System.Drawing.Point(0, 240)
        Me.TextBoxTestTo.Multiline = True
        Me.TextBoxTestTo.Name = "TextBoxTestTo"
        Me.TextBoxTestTo.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBoxTestTo.Size = New System.Drawing.Size(544, 240)
        Me.TextBoxTestTo.TabIndex = 4
        Me.TextBoxTestTo.Text = ""
        '
        'TextBoxTestFrom
        '
        Me.TextBoxTestFrom.Location = New System.Drawing.Point(0, 72)
        Me.TextBoxTestFrom.Multiline = True
        Me.TextBoxTestFrom.Name = "TextBoxTestFrom"
        Me.TextBoxTestFrom.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.TextBoxTestFrom.Size = New System.Drawing.Size(544, 160)
        Me.TextBoxTestFrom.TabIndex = 3
        Me.TextBoxTestFrom.Text = "<body>test testing er test</body>"
        '
        'TextBoxSok
        '
        Me.TextBoxSok.Location = New System.Drawing.Point(16, 112)
        Me.TextBoxSok.Name = "TextBoxSok"
        Me.TextBoxSok.Size = New System.Drawing.Size(224, 20)
        Me.TextBoxSok.TabIndex = 3
        Me.TextBoxSok.Text = "olje"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButtonDir)
        Me.GroupBox1.Controls.Add(Me.RadioButtonDep)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(160, 64)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Gruppe"
        '
        'RadioButtonDir
        '
        Me.RadioButtonDir.Location = New System.Drawing.Point(16, 40)
        Me.RadioButtonDir.Name = "RadioButtonDir"
        Me.RadioButtonDir.Size = New System.Drawing.Size(112, 16)
        Me.RadioButtonDir.TabIndex = 1
        Me.RadioButtonDir.Text = "Direktorater"
        '
        'RadioButtonDep
        '
        Me.RadioButtonDep.Checked = True
        Me.RadioButtonDep.Location = New System.Drawing.Point(16, 16)
        Me.RadioButtonDep.Name = "RadioButtonDep"
        Me.RadioButtonDep.Size = New System.Drawing.Size(112, 16)
        Me.RadioButtonDep.TabIndex = 0
        Me.RadioButtonDep.TabStop = True
        Me.RadioButtonDep.Text = "Departementer"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(16, 96)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 16)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "S�keord:"
        '
        'TextBoxDato
        '
        Me.TextBoxDato.Location = New System.Drawing.Point(16, 144)
        Me.TextBoxDato.Name = "TextBoxDato"
        Me.TextBoxDato.Size = New System.Drawing.Size(72, 20)
        Me.TextBoxDato.TabIndex = 6
        Me.TextBoxDato.Text = "2005.02.10"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(808, 526)
        Me.Controls.Add(Me.TextBoxDato)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TextBoxSok)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Alle dep: alle �r fra dato:
    'http://www2.interpost.no/cgi-bin/sift/epj/si-knapp?base=psd,jd,kad,md,sd,nud,ud,kd,kid,nhd,fd,ld,fid,bfd,oed,kuf,hd,sos&spm=(barnehage,+billig)+kl+(dato+sl+2005.01.15)&detalj=1+2+&sort=krono
    'Samme men OG s�k
    'http://www2.interpost.no/cgi-bin/sift/epj/si-knapp?base=psd,jd,kad,md,sd,nud,ud,kd,kid,nhd,fd,ld,fid,bfd,oed,kuf,hd,sos&spm=(barnehage)+kl+(fri)+kl+(dato+sl+2004.11.15)&detalj=1+2+3+&sort=krono

    'Alle Direktorater: 
    'http://www2.interpost.no/cgi-bin/sift/epj/si-knapp?base=ht,sft,nve,od,slt,ssb,lt,fsk,ft,dn,du,pod,hi,sb,sk,kot,mat,pt&spm=(test)+kl+(dato+sl+2005.01.28)&detalj=1+2+&sort=krono
    'Dim urlJournalSok As String = "http://www2.interpost.no/cgi-bin/sift/epj/si-knapp?base=bfd%2Coed%2Chd%2Csos&spm=%28energi%2C+fritak%29+kl+%28dato+sl+2005.01.20%29+kl+%28dato%3D2005.02*%29&detalj=1+2+&sort=krono"

    Dim urlJournalStart As String = "http://www2.interpost.no/cgi-bin/sift/epj/si-knapp"
    Dim urlJournalDep As String = "?base=psd,jd,kad,md,sd,nud,ud,kd,kid,nhd,fd,ld,fid,bfd,oed,kuf,hd,sos"
    Dim urlJournalDir As String = "?base=ht,sft,nve,od,slt,ssb,lt,fsk,ft,dn,du,pod,hi,sb,sk,kot,mat,pt"
    Dim urlJournalSpm As String = "&spm=(<sokeOrd/>)"
    Dim urlJournalDato As String = "+kl+(dato+sl+<dato/>)"
    Dim urlJournalDetalj As String = "&detalj=1+2+&sort=krono"

    Dim userName As String = "sintb98"
    Dim password As String = "bru38ke"
    Dim outPath As String = "..\test\"
    Dim fileName As String = "TestUthev.html"
    Dim objUthev As New UthevSok

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim sokeOrd As String = Me.TextBoxSok.Text   '"olje"
        Dim dato As String = Me.TextBoxDato.Text ' "2005.01.20"
        Dim minKunde As New Kunde

        'DoSokForKunde(minKunde.sokeOrd, minKunde.sisteSokDato, minKunde.epost)
        DoSokForKunde(sokeOrd, dato, minKunde.epost)
    End Sub

    Sub DoSokForKunde(ByVal sokeOrd As String, ByVal dato As String, ByVal epost As String)
        Dim strDoc As String
        Dim urlJournalSok As String
        Dim depDir As String
        Dim strDepDir As String

        If Me.RadioButtonDep.Checked Then
            depDir = urlJournalDep
            strDepDir = "departementene"
        Else
            depDir = urlJournalDir
            strDepDir = "direktoratene"
        End If

        urlJournalSok = urlJournalStart & depDir _
                & urlJournalSpm.Replace("<sokeOrd/>", sokeOrd) _
                & urlJournalDato.Replace("<dato/>", dato) _
                & urlJournalDetalj

        strDoc = GetUrlResponse(urlJournalSok, userName, password)

        LogFile.WriteFile(outPath & "Orig-" & fileName, strDoc, False)

        If Not strDoc.StartsWith("Error:") Then
            Me.TextBox2.Text = strDoc
            strDoc = RensHtml(strDoc)
            strDoc = objUthev.DoUthevSokParam(strDoc, sokeOrd, "", "", "<b><font style='background-color:ffff00'>$&</font></b>")
            strDoc = "<html><body><h4>Treffliste fra s�k med '" & sokeOrd & "' i basene til " & strDepDir _
                & "</h4>" & strDoc & "</body></html>"
            LogFile.WriteFile(outPath & fileName, strDoc, False)
        End If
        Me.TextBox1.Text = strDoc
        SendEpost(epost, strDoc)
    End Sub

    Function SendEpost(ByVal epost As String, ByVal tekst As String)
        Dim melding As New MailMessage
        melding.BodyFormat = MailFormat.Html
        melding.From = meldFrom
        melding.Subject = mailSubject
        melding.To = epost
        melding.Body = tekst

        Dim smtpSend As SmtpMail
        smtpSend.SmtpServer = smtpServer
        smtpSend.Send(melding)
    End Function

    Private Function RensHtml(ByVal strHtml As String) As String

        'strHtml = strHtml.Replace(vbCrLf, "")
        'strHtml = strHtml.Replace(Chr(10), "")
        'strHtml = strHtml.Replace(Chr(13), "")
        strHtml = strHtml.Replace("</", "##/")
        strHtml = strHtml.Replace("<", Chr(10) & "<")
        strHtml = strHtml.Replace("##/", "</")

        Dim linjer() As String

        linjer = strHtml.Split(Chr(10))

        Dim sbHtml As New System.Text.StringBuilder

        For Each linje As String In linjer

            If linje.StartsWith("<a href=""#Dok") Then
                sbHtml.Append(linje & vbCrLf)
            ElseIf linje.StartsWith(" 0") Then
                ' sbHtml.Append(linje & ": ")
            ElseIf linje.StartsWith("<B>") Then
                sbHtml.Append(linje & vbCrLf)
            ElseIf linje.StartsWith("<LI>") Then
                sbHtml.Append(linje & vbCrLf)
            ElseIf linje.StartsWith("SAK:") Then
                sbHtml.Append(linje & vbCrLf)
            ElseIf linje.StartsWith("<HR>") Then
                sbHtml.Append(linje & vbCrLf)
            ElseIf linje.StartsWith("Innhold ") Then
                sbHtml.Append(linje & vbCrLf)
            ElseIf linje.IndexOf(" dokument funnet") > -1 Then
                sbHtml.Append("<h4>" & linje & vbCrLf & "<PRE>")
            ElseIf linje.IndexOf(" funnet)") > -1 Then
                sbHtml.Append(linje & vbCrLf & "<PRE>")
            ElseIf linje.StartsWith("<A NAME=""DOKUMENTER"">") Then
                sbHtml.Append("</PRE><PRE>")
            ElseIf linje.StartsWith("<A NAME=""Dok") Then
                sbHtml.Append(linje & vbCrLf)
            End If
        Next

        Dim reg As New Regex("\</(.*?)\>|\<(.*?)\>")

        Dim strResult As String = reg.Replace(sbHtml.ToString, "")
        strResult = strResult.Replace(vbCrLf, "<br/>" & vbCrLf)

        Return strResult
    End Function

    Private Sub ButtonTestUthev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.TextBoxTestTo.Text = objUthev.DoUthevSok(Me.TextBoxTestFrom.Text, Me.TextBoxSok.Text)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim doc As String
        Dim docRes As String

        'objUthev.SetFileType(UthevSok.fType.html)
        doc = LogFile.ReadFile("..\test\test.html")
        'docRes = objUthev.UthevXml(doc, Me.TextBoxSok.Text)
        Me.TextBox1.Text = docRes
        LogFile.WriteFile("..\test\test-result.html", docRes, False)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim doc As String
        Dim docRes As String

        doc = LogFile.ReadFile("..\test\test.html")

        'docRes = objUthev.UthevRegEx(doc, Me.TextBoxSok.Text)

        docRes = GetStringPortion(doc, "<body class='viewBody'>", "</body>")
        docRes = objUthev.DoUthevSok(docRes, Me.TextBoxSok.Text)
        Me.TextBox1.Text = docRes
        docRes = BulkReplace(doc, "<body class='viewBody'>", "</body>", "<body class='viewBody'>" & docRes & "</body>")

        LogFile.WriteFile("..\test\test-result.html", docRes)

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click


        'If Not reg.IsMatch(exceptionList) Then
        Try
            Dim reg As New Regex(Me.TextBox3.Text, RegexOptions.IgnoreCase)
            Dim strReplace As String = Me.TextBox4.Text
            Me.TextBoxTestTo.Text = reg.Replace(Me.TextBoxTestFrom.Text, strReplace)
        Catch ex As Exception
            Me.TextBoxTestTo.Text = ex.Message
        End Try

    End Sub
End Class
