Module ModuleFunctions

    'Replaces everything between two tags!
    Function BulkReplace(ByRef strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, Optional ByVal intStart As Integer = 0, Optional ByVal intCount As Integer = -1) As String
        'Replaces everything between two tags!
        Dim i As Integer
        Dim intLastStart As Integer = 0
        Dim intEnd As Integer = 0
        Dim sbTemp As New System.Text.StringBuilder

        If strFind = "" Or strFind2 = "" Then Return strDoc

        ' Allow max 10000 susbstitutions, if set to -1
        If intCount < 0 Then
            intCount = 10000
        End If

        intLastStart = 0

        For i = 1 To intCount
            intStart = strDoc.IndexOf(strFind, intStart)
            If intStart = -1 Then Exit For

            intEnd = strDoc.IndexOf(strFind2, intStart + Len(strFind))
            If intEnd = -1 Then Exit For

            sbTemp.Append(strDoc.Substring(intLastStart, intStart - intLastStart) & strReplace)

            intStart = intEnd + Len(strFind2)
            intLastStart = intStart
        Next

        If intLastStart = 0 Then
            'No string pair found, do nothing
            Return strDoc
        Else
            'Add last part of string
            sbTemp.Append(strDoc.Substring(intLastStart))
            'strDoc = sbTemp.ToString
            Return sbTemp.ToString
        End If
    End Function

    ' Replaces all paired start and end tags!
    Function PairReplace(ByRef strDoc As String, ByVal strFind As String, ByVal strFind2 As String, ByVal strReplace As String, ByVal strReplace2 As String, Optional ByVal intStart As Integer = 0, Optional ByVal intCount As Integer = -1) As String
        Dim i As Integer
        Dim intLastStart As Integer = 0
        Dim intEnd As Integer = 0
        Dim sbTemp As System.Text.StringBuilder = New System.Text.StringBuilder

        If strFind = "" Or strFind2 = "" Then Return strDoc

        ' Allow max 10000 susbstitutions, if set to -1
        If intCount < 0 Then
            intCount = 100000
        End If

        For i = 1 To intCount
            intStart = strDoc.IndexOf(strFind, intStart)
            If intStart = -1 Then Exit For

            intEnd = strDoc.IndexOf(strFind2, intStart + Len(strFind))
            If intEnd = -1 Then Exit For

            sbTemp.Append(strDoc.Substring(intLastStart, intStart - intLastStart) & strReplace & strDoc.Substring(intStart + Len(strFind), intEnd - intStart - Len(strFind)) & strReplace2)

            intStart = intEnd + Len(strFind2)
            intLastStart = intStart
        Next

        If intLastStart = 0 Then
            'No string pair found, do nothing
            Return strDoc
        Else
            'Add last part of string
            sbTemp.Append(strDoc.Substring(intLastStart))
            Return sbTemp.ToString
        End If
    End Function

    Function GetStringPortion(ByRef strString As String, ByVal strStartTag As String, ByVal strEndTag As String) As String
        ' Find Portion of a text-string found between two tags (substrings)
        ' If not both tags are found the function returns an empty string

        Dim intStart, intEnd As Integer

        intStart = strString.IndexOf(strStartTag)
        If intStart = -1 Then
            Return ""
        End If
        intStart += strStartTag.Length

        intEnd = strString.IndexOf(strEndTag, intStart)
        If intEnd = -1 Then
            Return ""
        End If

        Return strString.Substring(intStart, intEnd - (intStart))
    End Function

End Module

