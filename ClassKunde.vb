Public Class Kunde
    'With default values
    Private strNavn As String = "Roar Vestre"
    Private strSokeord As String = "l�sning"
    Private strEpost As String = "roar.vestre@start.no"
    'Private strEpost As String = "lch@ntb.no"
    Private dtSisteSokDato As String = "2005.02.01"
    Private strKontakt As String = "Roar Vestre"
    Private strMobile As String = "93053268"
    Private strUserName As String = ""
    Private strPassword As String = ""

    Sub New()
        'MyClass.Init()
    End Sub

    Private Sub Init()

    End Sub

    Public Property navn() As String
        Get ' Retrieves the property value.
            Return strNavn
        End Get
        Set(ByVal Value As String)
            strNavn = Value
        End Set
    End Property

    Public Property sokeOrd() As String
        Get ' Retrieves the property value.
            Return strSokeord
        End Get
        Set(ByVal Value As String)
            strSokeord = Value
        End Set
    End Property

    Public Property epost() As String
        Get ' Retrieves the property value.
            Return strEpost
        End Get
        Set(ByVal Value As String)
            strEpost = Value
        End Set
    End Property

    Public Property sisteSokDato() As String
        Get ' Retrieves the property value.
            Return dtSisteSokDato
        End Get
        Set(ByVal Value As String)
            dtSisteSokDato = Value
        End Set
    End Property
End Class
