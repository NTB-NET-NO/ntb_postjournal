Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports System.Xml
Imports System.Web.HttpUtility
Imports ntb_FuncLib

Module ModuleMain


#Region "Globale variables"

    'Query
    Private urlGrunnspill As String
    Private urlSluttspill As String

    'Folders
    Private logFiles As String
    Private queryLog As String
    Private xmlOut As String
    Private satOutFolder As String
    Private ftpOutFolder As String

    'Files
    Private versionFile As String
    Private xsltFile As String
    Private xsltEnkeltres As String

    'Logfiles
    'Private systemLog As StreamWriter
    'Private errorLog As StreamWriter

    Private versionNr As Integer 'Last Run ID

    'Other
    Private xmlDocVersion As XmlDocument 'Reusable XML doc

    Private IPTC_Sequence As Integer
    Private IPTC_SequenceFile As String
    Private IPTC_Range As String

#End Region

    Public Sub Main()
        Try
            Init()

            RunJob("grunnspill")
            RunJob("sluttspill")

            RunJob("grunnspill", "AFT")
            RunJob("sluttspill", "AFT")

        Catch err As Exception

            LogFile.WriteErr(logFiles, "Feil i Form1_Load", err)
        End Try
    End Sub

    Private Sub RunJob(ByVal tabType As String, optional avis as String = "ALL")
        Dim strDoc As String
        If tabType = "grunnspill" Then
            strDoc = GetUrlResponse(urlGrunnspill)
        Else
            strDoc = GetUrlResponse(urlSluttspill)
        End If

        If strDoc.Trim <> "" Then
            GetVersion(tabType)
            Dim dt As DateTime = Now
            Dim strDocXml As String
            strDocXml = LagNitf(strDoc, "Resultater Norway Cup - " & tabType, "fotb-NC-" & tabType.Substring(0, 5) & "-res", versionNr, dt, avis)

            'LagIptc(strDocXml, tabType, satOutFolder, "sat", "NorwayCup-" & tabType)
            'LagIptc(strDocXml, tabType, ftpOutFolder, "ftp", "NorwayCup-" & tabType)

            If tabType = "grunnspill" Then
                strDocXml = LagEnkeltres(strDocXml)

                Dim filename As String = dt.ToString("yyyy-MM-dd_HH-mm-ss") & "_fotb-NC-" & tabType.Substring(0, 5) & "-enkeltres" & ".xml"
                WriteXmlFile(strDocXml, filename)

                'LagIptc(strDocXml, tabType, satOutFolder, "sat", "NorwayCup-enkeltres-" & tabType)
                'LagIptc(strDocXml, tabType, ftpOutFolder, "ftp", "NorwayCup-enkeltres-" & tabType)
            End If

            xmlDocVersion.Save(versionFile)
        End If
    End Sub

    Function GetCommandLineArgs() As String()
        ' Declare variables.
        Dim separators As String = " "
        Dim commands As String = Microsoft.VisualBasic.Command()
        Dim args() As String = commands.Split(separators.ToCharArray)
        Return args
    End Function

    Private Sub GetVersion(ByVal tabType As String)
        Dim nodeCount As XmlNode = xmlDocVersion.SelectSingleNode("/doc/" & tabType & "/run-count")
        Dim nodeLast As XmlNode = xmlDocVersion.SelectSingleNode("/doc/" & tabType & "/last-run")
        If nodeLast.InnerText = Format(Today, "yyyy.MM.dd") Then
            versionNr = nodeCount.InnerText + 1
            nodeCount.InnerText = versionNr
        Else
            nodeCount.InnerText = 0
            nodeLast.InnerText = Format(Today, "yyyy.MM.dd")
            versionNr = 0
        End If
    End Sub

    'Load settings and initiate server
    Private Sub Init()
        'Init vars
        xmlDocVersion = New XmlDocument

        'Load settings
        logFiles = AppSettings("logFileFolder")
        xmlOut = AppSettings("xmlOutFolder")

        versionFile = AppSettings("versionFile")
        'xsltFile = AppSettings("xsltFile")
        xsltEnkeltres = AppSettings("xsltEnkeltres")

        'Query settings
        urlGrunnspill = AppSettings("urlGrunnspill")
        urlSluttspill = AppSettings("urlSluttspill")

        Directory.CreateDirectory(logFiles)
        Directory.CreateDirectory(xmlOut)

        'Log event
        LogFile.WriteLog(logFiles, "-- NorwayCup: Logging started --")

        Directory.CreateDirectory(Path.GetDirectoryName(versionFile))

        If Not File.Exists(versionFile) Then
            Dim strVersionDoc As String
            strVersionDoc = "<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf _
                    & "<doc>" & vbCrLf _
                    & "<grunnspill>" & vbCrLf _
                    & "<last-run>2003.01.01</last-run>" & vbCrLf _
                    & "<run-count>0</run-count>" & vbCrLf _
                    & "</grunnspill>" & vbCrLf _
                    & "<sluttspill>" & vbCrLf _
                    & "<last-run>2003.01.01</last-run>" & vbCrLf _
                    & "<run-count>0</run-count>" & vbCrLf _
                    & "</sluttspill>" & vbCrLf _
                    & "</doc>" & vbCrLf
            LogFile.WriteFile(versionFile, strVersionDoc)
            versionNr = 0
        End If
        xmlDocVersion.Load(versionFile)

        IPTC_Range = AppSettings("IPTCRange")
        IPTC_SequenceFile = AppSettings("IPTCSequenceFile")

        IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        LoadSequence(IPTC_SequenceFile, True)


    End Sub

    Private Sub LoadSequence(ByVal file As String, ByVal load As Boolean)
        Try
            If load Then
                IPTC_Sequence = LogFile.ReadFile(file).Trim()
                LogFile.WriteLog(logFiles, "IPTC Sequence number loaded: " & IPTC_Sequence)
            Else
                LogFile.WriteFile(file, IPTC_Sequence, False)
                LogFile.WriteLog(logFiles, "IPTC Sequence number saved: " & IPTC_Sequence)
            End If
        Catch ex As Exception
            LogFile.WriteErr(logFiles, "Error accessing IPTC sequence number.", ex)
        End Try
    End Sub

    Private Function Get_Seq() As Integer

        'Check value
        If IPTC_Sequence > CInt(IPTC_Range.Split("-")(1)) Or _
           IPTC_Sequence < CInt(IPTC_Range.Split("-")(0)) Then
            IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        End If

        'Return value
        Dim ret As Integer = IPTC_Sequence

        'Increment
        IPTC_Sequence += 1

        'Save
        LoadSequence(IPTC_SequenceFile, False)

        Return ret
    End Function

    Public Function GetUrlResponse(ByVal URL As String, Optional ByVal userName As String = "", Optional ByVal password As String = "") As String

        Dim ok As Boolean = True
        Dim ret As String

        'Create the URI
        Dim uri As New UriBuilder(URL)
        'Dim uri As New System.Uri(URL)

        uri.UserName = userName
        uri.Password = password

        'Create the HTTP request
        Dim request As HttpWebRequest = WebRequest.Create(uri.Uri)

        request.Method = "GET"
        request.Timeout = 180000

        ' Create a New 'NetworkCredential' object.
        Dim networkCredential As New NetworkCredential(userName, password)

        ' Associate the 'NetworkCredential' object with the 'WebRequest' object.
        request.Credentials = networkCredential


        'Get the HTTP response with the resulting data
        Dim response As HttpWebResponse
        Try
            response = request.GetResponse()
        Catch webEx As WebException
            'Log WEB error
            LogFile.WriteErr(logFiles, "HTTP request failed: ", webEx)
            'If Not webEx.Response Is Nothing Then webEx.Response.Close()
            Return "Error: " & webEx.Message
        End Try

        Dim reader As StreamReader = New StreamReader(response.GetResponseStream(), Encoding.GetEncoding("iso-8859-1"))
        ret = reader.ReadToEnd
        reader.Close()

        'ret = ret.Replace("COOP", "")
        'ret = ret.Replace("Coop", "")
        Return ret
    End Function

    ''Logs an exception to error-log.txt
    'Sub LogError(ByVal ex As Exception)

    '    Try
    '        errorLog = New StreamWriter(logFiles & "error-log.txt", True, Encoding.GetEncoding("iso-8859-1"))
    '        errorLog.WriteLine(Now & " : " & ex.Message)
    '        errorLog.WriteLine(ex.StackTrace & vbCrLf & vbCrLf)
    '        errorLog.Close()
    '    Catch e As Exception
    '    End Try

    'End Sub

    Private Function LagNitf(ByVal strDoc As String, ByVal strHedline As String, ByVal keyWord As String, ByVal versjon As Integer, ByVal dt As DateTime, Optional ByVal avis As String = "ALL") As String
        Dim arrDoc() As String = Split(strDoc, vbCrLf)
        Dim strTableType As String = ""

        Dim strNy As String = ""
        If versjon > 0 Then
            strNy = "ny" & versjon & "-"
        End If

        Dim filename As String = dt.ToString("yyyy-MM-dd_HH-mm-ss") & "_" & keyWord & ".xml"

        Dim addId As String = "10"
        If keyWord.IndexOf("slutt") > -1 Then addId = "30"

        Dim sbDoc As New StringBuilder

        sbDoc.Append("<?xml version='1.0' encoding='iso-8859-1' standalone='yes'?>" & vbCrLf)
        sbDoc.Append("<nitf version='-//IPTC-NAA//DTD NITF-XML 3.2//EN' change.date='October 10, 2003' change.time='19:30' baselang='no-NO'>" & vbCrLf)

        sbDoc.Append("<head>" & vbCrLf)
        sbDoc.Append("<title>Resultater Norway Cup</title>" & vbCrLf)

        'META headers
        sbDoc.Append("<meta name=""timestamp"" content=""" & dt.ToString("yyyy.MM.dd HH:mm:ss") & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""foldername"" content=""Ut-Satellitt"" />" & vbCrLf)
        sbDoc.Append("<meta name=""subject"" content=""" & strNy & keyWord & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""filename"" content=""" & filename & """ />" & vbCrLf)

        sbDoc.Append("<meta name=""NTBTjeneste"" content=""Nyhetstjenesten"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBMeldingsSign"" content=""nc"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBPrioritet"" content=""5"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBStikkord"" content=""" & keyWord & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBDistribusjonsKode"" content=""" & avis & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBKanal"" content=""A"" />" & vbCrLf)

        sbDoc.Append("<meta name=""NTBSequenceNumber"" content=""" & Get_Seq() & """ />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBID"" content=""RED" & Format(Now, "yyyyMMddHHmmss") & "_" & addId & "_NC"" />" & vbCrLf)
        sbDoc.Append("<meta name=""NTBMeldingsType"" content=""Sportstabeller"" />" & vbCrLf)
        sbDoc.Append("<meta name=""ntb-dato"" content=""" & dt.ToString("dd.MM.yyyy HH:mm") & """ />" & vbCrLf)

        'TOBJECT
        sbDoc.Append("<tobject tobject.type=""Sport"">" & vbCrLf)
        sbDoc.Append("<tobject.property tobject.property.type=""Tabeller og resultater"" />" & vbCrLf)
        sbDoc.Append("<tobject.subject tobject.subject.code=""SPO"" tobject.subject.refnum=""15000000"" tobject.subject.type=""Sport"" />" & vbCrLf)
        sbDoc.Append("<tobject.subject tobject.subject.code=""SPO"" tobject.subject.refnum=""15040000"" tobject.subject.matter=""Fotball"" />" & vbCrLf)
        sbDoc.Append("</tobject>" & vbCrLf)

        'DOCDATA
        sbDoc.Append("<docdata>" & vbCrLf)
        sbDoc.Append("<evloc state-prov=""Norge"" />" & vbCrLf)
        sbDoc.Append("<evloc state-prov=""Norge"" county-dist=""Oslo"" />" & vbCrLf)
        sbDoc.Append("<evloc state-prov=""Norge"" county-dist=""Riksnyheter"" />" & vbCrLf)

        sbDoc.Append("<doc-id regsrc=""NTB"" id-string=""RED" & dt.ToString("yyyyMMddHHmmss") & "_" & addId & "_NC_0" & versjon & """ />" & vbCrLf)
        sbDoc.Append("<urgency ed-urg=""5"" />" & vbCrLf)
        sbDoc.Append("<date.issue norm=""" & dt.ToString("yyyy-MM-ddTHH:mm:ss") & """ />" & vbCrLf)
        sbDoc.Append("<ed-msg info="""" />" & vbCrLf)
        sbDoc.Append("<du-key version=""" & versjon + 1 & """ key=""" & keyWord & """ />" & vbCrLf)
        sbDoc.Append("<doc.copyright year=""" & dt.ToString("yyyy") & """ holder=""NTB"" />" & vbCrLf)
        sbDoc.Append("<key-list>" & vbCrLf)
        sbDoc.Append("<keyword key=""" & keyWord & """ />" & vbCrLf)
        sbDoc.Append("</key-list>" & vbCrLf)
        sbDoc.Append("</docdata>" & vbCrLf)

        sbDoc.Append("<pubdata date.publication=""" & dt.ToString("yyyyMMddTHHmmss") & """ item-length=""" & strDoc.Length + strHedline.Length & """ unit-of-measure=""character"" />" & vbCrLf)
        sbDoc.Append("<revision-history name=""nc"" />" & vbCrLf)

        sbDoc.Append("</head>" & vbCrLf)

        'BODY
        sbDoc.Append("<body>" & vbCrLf)

        sbDoc.Append("<body.head>" & vbCrLf)
        sbDoc.Append("<hedline>" & vbCrLf)
        sbDoc.Append("<hl1>" & strHedline & "</hl1>" & vbCrLf)
        sbDoc.Append("</hedline>" & vbCrLf)
        sbDoc.Append("<distributor><org>NTB</org></distributor>" & vbCrLf)
        sbDoc.Append("</body.head>" & vbCrLf)
        sbDoc.Append("<body.content>" & vbCrLf)

        Dim firstLine As Boolean = True
        Dim bTable As Boolean = False

        Dim line As String
        For Each line In arrDoc
            line = getXmlEntity(line)
            If line.IndexOf(vbTab) = -1 Then
                ' Vanlige linjer (ikke tabell)
                If bTable Then
                    bTable = False
                    sbDoc.Append("</table>" & vbCrLf)
                End If
                If firstLine Then
                    ' F�rste linje blir ingress:
                    If line <> "" Then
                        sbDoc.Append("<p class=""table-code"">[t01]</p>" & vbCrLf)
                        ' Hopper over f�rste linje
                        'sbDoc.Append("<p lede=""true"">" & line & "</p>" & vbCrLf)
                        'sbDoc.Append("<p>" & line & "</p>" & vbCrLf)
                        firstLine = False
                    End If
                ElseIf line.StartsWith("Norway Cup ") Or line.StartsWith("Klasse ") Then

                    If avis = "AFT" Then
                        sbDoc.Append("<hl2>" & line & "</hl2>" & vbCrLf)
                    Else
                        sbDoc.Append("<p class=""table-code"">[t01u1]</p>" & vbCrLf)
                        sbDoc.Append("<table class=""1-col"">" & vbCrLf)
                        sbDoc.Append("<tr>" & vbCrLf)
                        sbDoc.Append("<td>" & line & "</td>" & vbCrLf)
                        sbDoc.Append("</tr>" & vbCrLf)
                        sbDoc.Append("</table>" & vbCrLf)
                    End If

                ElseIf line <> "" Then
                    sbDoc.Append("<p>" & line & "</p>" & vbCrLf)
                End If
            Else
                ' Tabell Linjer (inneholder tab)
                Dim arrLine() As String = Split(line, vbTab)
                Dim col As String
                Dim firstCol As Boolean = True


                If Not bTable Then
                    If arrLine.Length = 4 Then
                        strTableType = "t01u2"
                    ElseIf arrLine.Length = 9 Then
                        strTableType = "t01u5"
                    Else
                        strTableType = "t01u" & arrLine.Length
                    End If

                    sbDoc.Append("<p class=""table-code"">[" & strTableType & "]</p>" & vbCrLf)
                    'sbDoc.Append("<table class=""" & strTableType & """ border=""1""  cellspacing=""0"" cellpadding=""3"">" & vbCrLf)
                    sbDoc.Append("<table class=""" & arrLine.Length & "-col"">" & vbCrLf)
                    bTable = True
                End If

                ' Ny linje
                sbDoc.Append("<tr>" & vbCrLf)
                For Each col In arrLine
                    ' Kolonner i tabell
                    If firstCol Then
                        sbDoc.Append("<td>")
                        firstCol = False
                    ElseIf col = "-" And strTableType = "t01u2" Then
                        sbDoc.Append("<td align=""center"">")
                    ElseIf col = "-" Then
                        sbDoc.Append("<td>")
                    Else
                        sbDoc.Append("<td align=""right"">")
                    End If

                    sbDoc.Append(col)
                    sbDoc.Append("</td>" & vbCrLf)
                Next
                sbDoc.Append("</tr>" & vbCrLf)
            End If
        Next

        If bTable Then
            bTable = False
            sbDoc.Append("</table>" & vbCrLf)
        End If

        sbDoc.Append("</body.content>" & vbCrLf)

        sbDoc.Append("<body.end>" & vbCrLf)
        sbDoc.Append("<tagline>" & vbCrLf)
        sbDoc.Append("<a href=""mailto:sporten@ntb.no"">sporten@ntb.no</a>" & vbCrLf)
        sbDoc.Append("</tagline>" & vbCrLf)
        sbDoc.Append("</body.end>" & vbCrLf)

        sbDoc.Append("</body>" & vbCrLf)
        sbDoc.Append("</nitf>" & vbCrLf)

        WriteXmlFile(sbDoc.ToString, filename)

        Return sbDoc.ToString
    End Function

    Private Function getXmlEntity(ByVal strLine As String) As String

        strLine = strLine.Replace("&", "&amp;")
        strLine = strLine.Replace("<", "&lt;")
        strLine = strLine.Replace(">", "&gt;")
        'strLine = strLine.Replace("""", "&quot;")
        'strLine = strLine.Replace("'", "&apos;")

        Return strLine
    End Function

    Private Sub WriteXmlFile(ByVal strDoc As String, ByVal strFileName As String)
        Dim strFile As String = xmlOut & "/" & strFileName
        LogFile.WriteFile(strFile, strDoc)
    End Sub

    Private Sub LagIptc(ByVal strDoc As String, ByVal tabType As String, ByVal strOutFolder As String, ByVal sendetype As String, ByVal strFileNameHead As String)
        Dim xmlNitfDoc As New System.Xml.XmlDocument
        Dim xslTransform As New System.Xml.Xsl.XslTransform

        xmlNitfDoc.LoadXml(strDoc)

        xslTransform.Load(xsltFile)
        Dim xsltArgs As New Xsl.XsltArgumentList

        xsltArgs.AddParam("sendetype", "", sendetype)

        Dim s As New IO.MemoryStream
        xslTransform.Transform(xmlNitfDoc, xsltArgs, s, Nothing)
        s.Position = 0
        Dim sr As New IO.StreamReader(s, Encoding.GetEncoding(1252))

        Dim strTemp1 As String = sr.ReadToEnd
        Dim strTemp2 As String = Replace2ITPC(strTemp1)

        Dim w As New StreamWriter(strOutFolder & "\" & strFileNameHead & "_" & Format(Now, "yyyy-MM-dd_HH-mm-ss") & ".NTA", False, System.Text.Encoding.GetEncoding(1252))
        w.Write(strTemp2)
        w.Flush()   '  update underlying file
        w.Close()   '  close the writer and underlying file

        'LogFile.WriteFile(xmlOut & "\test.nta", strTemp2)

    End Sub

    Private Function LagEnkeltres(ByVal strDoc As String) As String
        Dim xmlNitfDoc As New System.Xml.XmlDocument
        Dim xslTransform As New System.Xml.Xsl.XslTransform

        strDoc = strDoc.Replace("Resultater Norway Cup", "Enkeltresultater Norway Cup")
        strDoc = strDoc.Replace("-grunn-res", "-grunn-enkeltres")
        strDoc = strDoc.Replace("_10_NC", "_20_NC")

        Dim seq As Integer = Get_Seq()
        strDoc = strDoc.Replace("<meta name=""NTBSequenceNumber"" content=""" & seq - 1 & """ />", "<meta name=""NTBSequenceNumber"" content=""" & seq & """ />")

        xmlNitfDoc.PreserveWhitespace = True
        xmlNitfDoc.LoadXml(strDoc)

        xslTransform.Load(xsltEnkeltres)

        'Dim xsltArgs As New Xsl.XsltArgumentList()
        'xsltArgs.AddParam("sendetype", "", sendetype)

        Dim s As New IO.MemoryStream
        xslTransform.Transform(xmlNitfDoc, Nothing, s, Nothing)
        s.Position = 0
        Dim sr As New IO.StreamReader(s, Encoding.GetEncoding(1252))
        Return sr.ReadToEnd

    End Function

    Function Replace2ITPC(ByVal strInput As String) As String
        Dim sb As New StringBuilder(strInput)

        sb.Replace(Chr(13) & Chr(10), "")
        sb.Replace("{crlf}", Chr(13) & Chr(10))
        sb.Replace(Chr(196), "&#200;A")
        sb.Replace(Chr(195), Chr(196) & "A")
        sb.Replace(Chr(194), Chr(195) & "A")
        sb.Replace(Chr(193), Chr(194) & "A")
        sb.Replace(Chr(200), Chr(193) & "E")
        sb.Replace("&#200;", Chr(193))
        sb.Replace(Chr(225), Chr(194) & "a")
        sb.Replace(Chr(201), Chr(194) & "E")
        sb.Replace(Chr(233), Chr(194) & "e")
        sb.Replace(Chr(211), Chr(194) & "O")
        sb.Replace(Chr(243), Chr(194) & "o")
        sb.Replace(Chr(218), Chr(194) & "U")
        sb.Replace(Chr(250), Chr(194) & "u")
        sb.Replace(Chr(221), Chr(194) & "Y")
        sb.Replace(Chr(253), Chr(194) & "y")
        sb.Replace(Chr(192), Chr(193) & "A")
        sb.Replace(Chr(224), Chr(193) & "a")
        sb.Replace(Chr(232), Chr(193) & "e")
        sb.Replace(Chr(210), Chr(193) & "O")
        sb.Replace(Chr(242), Chr(193) & "o")
        sb.Replace(Chr(217), Chr(193) & "U")
        sb.Replace(Chr(249), Chr(193) & "u")
        sb.Replace(Chr(226), Chr(195) & "a")
        sb.Replace(Chr(202), Chr(195) & "E")
        sb.Replace(Chr(234), Chr(195) & "e")
        sb.Replace(Chr(238), Chr(195) & "i")
        sb.Replace(Chr(206), Chr(195) & "I")
        sb.Replace(Chr(212), Chr(195) & "O")
        sb.Replace(Chr(244), Chr(195) & "o")
        sb.Replace(Chr(219), Chr(195) & "U")
        sb.Replace(Chr(251), Chr(195) & "u")
        sb.Replace(Chr(227), Chr(196) & "a")
        sb.Replace(Chr(209), Chr(196) & "N")
        sb.Replace(Chr(241), Chr(196) & "n")
        sb.Replace(Chr(213), Chr(196) & "O")
        sb.Replace(Chr(245), Chr(196) & "o")
        sb.Replace(Chr(228), Chr(200) & "a")
        sb.Replace(Chr(207), Chr(200) & "I")
        sb.Replace(Chr(239), Chr(200) & "i")
        sb.Replace(Chr(214), Chr(200) & "O")
        sb.Replace(Chr(246), Chr(200) & "o")
        sb.Replace(Chr(220), Chr(200) & "U")
        sb.Replace(Chr(252), Chr(200) & "u")
        sb.Replace(Chr(255), Chr(200) & "y")
        sb.Replace(Chr(36), "&#164;")
        sb.Replace(Chr(164), Chr(36))
        sb.Replace("&#164;", Chr(36))
        sb.Replace(Chr(91), Chr(154))
        sb.Replace(Chr(93), Chr(159))
        sb.Replace(Chr(127), Chr(0))
        sb.Replace(Chr(146), Chr(39))
        sb.Replace(Chr(148), Chr(34))
        sb.Replace(Chr(150), Chr(45))
        sb.Replace(Chr(170), Chr(65))
        sb.Replace(Chr(176), Chr(0))
        sb.Replace(Chr(184), Chr(44))
        sb.Replace(Chr(199), Chr(67))
        sb.Replace(Chr(215), Chr(180))
        sb.Replace(Chr(229), Chr(166))
        sb.Replace(Chr(231), Chr(99))
        sb.Replace(Chr(247), Chr(184))
        sb.Replace(Chr(153), Chr(212))
        sb.Replace(Chr(168), Chr(200))
        sb.Replace(Chr(169), Chr(211))
        sb.Replace(Chr(174), Chr(210))
        sb.Replace(Chr(186), Chr(202))
        sb.Replace(Chr(197), Chr(168))
        sb.Replace(Chr(198), Chr(225))
        sb.Replace(Chr(208), Chr(226))
        sb.Replace(Chr(216), Chr(233))
        sb.Replace(Chr(223), Chr(251))
        sb.Replace(Chr(230), Chr(241))
        sb.Replace(Chr(240), Chr(243))
        sb.Replace(Chr(248), Chr(249))
        sb.Replace(Chr(254), Chr(252))
        sb.Replace("&#x99;", Chr(153))
        sb.Replace("&#x01;", Chr(1))
        sb.Replace("&#x02;", Chr(2))
        sb.Replace("&#x03;", Chr(3))
        sb.Replace("&#x04;", Chr(4))
        sb.Replace("&#x80;", Chr(128))
        sb.Replace("&#x90;", Chr(144))
        sb.Replace("&#x92;", Chr(146))
        sb.Replace("&#x94;", Chr(148))
        sb.Replace("&#x9E;", Chr(158))
        sb.Replace("&#x98;", Chr(152))
        sb.Replace("&#xB6;", Chr(182))
        sb.Replace("&#x0D;", Chr(13))
        sb.Replace("&#x0A;", Chr(10))
        sb.Replace("&#128;", Chr(128))
        sb.Replace("&#132;", Chr(132))
        sb.Replace("&#134;", Chr(134))
        sb.Replace("&#133;", Chr(133))
        sb.Replace("&#136;", Chr(136))
        sb.Replace("&#135;", Chr(135))
        sb.Replace("&#148;", Chr(148))
        sb.Replace("&#149;", Chr(149))
        sb.Replace("&#154;", Chr(154))
        sb.Replace("&#159;", Chr(159))
        sb.Replace("&#208;", Chr(208))
        Return sb.ToString
    End Function

End Module
