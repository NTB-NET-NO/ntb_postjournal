Imports System.Text.RegularExpressions
Imports System.Xml

Public Class TestUthevSok

    'Private startHtml As String = "<font color='red'><b>" ' red = #FF0000
    'Private endHtml As String = "</b></font>"
    Private startHtml As String = "<font class='viewUthev'>"
    Private endHtml As String = "</font>"

    Private startXML As String = "<em style='keyword'>"
    Private endXML As String = "</em>"

    Private startCode As String
    Private endCode As String
    Enum fType
        xml = 1
        html = 2
    End Enum

    Public Sub New(ByVal fileType As fType)
        SetFileType(fileType)
    End Sub

    Public Sub SetFileType(ByVal fileType As fType)
        Select Case fileType
            Case fType.xml
                startCode = startXML
                endCode = endXML
            Case fType.html
                startCode = startHtml
                endCode = endHtml
            Case Else
                startCode = "<b>"
                endCode = "</b>"
        End Select

    End Sub


    Public Function Uthev(ByVal doc As String, ByVal sokeord As String) As String
        doc = doc.Replace(sokeord, startCode & sokeord & endCode)

        Return doc
    End Function

    Public Function UthevNoCase(ByVal doc As String, ByVal sokeord As String) As String
        Dim startPos As Long = 1
        Dim lastPos As Long = 1
        Dim sbDoc As New System.Text.StringBuilder
        Dim len As Integer = sokeord.Length

        Do
            Dim temp1 As String
            Dim temp2 As String
            startPos = InStr(lastPos, doc, sokeord, CompareMethod.Text)
            If startPos = 0 Then
                sbDoc.Append(doc.Substring(lastPos))
                Exit Do
            Else
                temp1 = doc.Substring(lastPos, startPos - lastPos - 1)
                temp2 = doc.Substring(startPos - 1, len)

                sbDoc.Append(temp1 & startCode & temp2 & endCode)
                lastPos = startPos + len - 1
            End If
        Loop
        Return sbDoc.ToString
    End Function

    Public Function UthevNoCaseWord(ByVal doc As String, ByVal sokeord As String) As String
        Dim startPos As Long = 1
        Dim lastPos As Long = 1
        Dim sbDoc As New System.Text.StringBuilder
        Dim len As Integer = sokeord.Length

        Do
            Dim temp1 As String
            Dim temp2 As String
            Dim temp3 As String

            startPos = InStr(lastPos, doc, " " & sokeord & " ", CompareMethod.Text)

            If startPos = 0 Then
                If doc.Length > 0 Then
                    If lastPos = 1 Then lastPos = 0
                    sbDoc.Append(doc.Substring(lastPos))
                End If
                Exit Do
            Else
                If lastPos = 1 Then lastPos = 0
                temp1 = doc.Substring(lastPos, startPos - lastPos)
                temp2 = doc.Substring(startPos, len)
                temp3 = temp1 & startCode & temp2 & endCode

                sbDoc.Append(temp1 & startCode & temp2 & endCode)
                lastPos = startPos + len
            End If
        Loop
        Return sbDoc.ToString
    End Function

    Public Function UthevRegEx(ByVal doc As String, ByVal sokeord As String) As String
        ' For uthevning av s�keord i HTML fil 
        ' Flere s�keord kan bringes inn kommaseparert.

        If sokeord = "" Or sokeord.Length <= 2 Then
            Return doc
        End If

        Dim docRes As String
        Dim startPattern As String = "(\b"
        ' sluttPattern med norsk stemming av substantiver:
        Dim sluttPattern As String = "(er|ene|e|en|et|a|s|ne|t|r)?\b)"
        Dim sok As String

        sokeord = sokeord.Replace(" ", ",")
        Dim sokeOrdliste() As String = Split(sokeord, ",")
        ' exceptionList for html code-ord
        Dim exceptionList As String = " div class href mailto subject "

        For Each strSok As String In sokeOrdliste
            strSok = strSok.Trim
            If exceptionList.IndexOf(" " & strSok & " ") = -1 And strSok.Length > 1 Then
                sok &= "|" & strSok
            End If
        Next
        sok = sok.Substring(1)

        'sok = sokeord.Replace(", ", "|")
        'sok = sok.Replace(",", "|")

        sok = "(" & sok & ")"

        Dim strReplace As String = startCode & "$&" & endCode

        docRes = doc

        Dim reg As New Regex(startPattern & sok & sluttPattern, RegexOptions.IgnoreCase)

        docRes = reg.Replace(docRes, strReplace)

        Return docRes

    End Function

    Public Function UthevXml(ByVal doc As String, ByVal sokeord As String) As String
        If sokeord = "" Or sokeord.Length <= 2 Then
            Return doc
        End If

        Dim docRes As String
        Dim startPattern As String = "(\b"
        ' sluttPattern med norsk stemming av substantiver:
        Dim sluttPattern As String = "(er|ene|e|en|et|a|s|ne|t|r)?\b)"
        Dim sok As String

        sok = sokeord.Replace(", ", "|")
        sok = sok.Replace(",", "|")
        sok = "(" & sok & ")"

        Dim strReplace As String = startCode & "$&" & endCode

        Dim reg As New Regex(startPattern & sok & sluttPattern, RegexOptions.IgnoreCase)

        Dim xmlDoc As New XmlDocument
        xmlDoc.PreserveWhitespace = True
        Try
            xmlDoc.LoadXml(doc)

            Dim nodeList As XmlNodeList = xmlDoc.SelectNodes("/html/body//div[@class='viewHeadline' or @class='viewIngress' or @class='viewIndent' or @class='viewNews' or @class='viewHl2']")

            For Each node As XmlNode In nodeList
                node.InnerXml = reg.Replace(node.InnerXml, strReplace)
            Next
        Catch ex As Exception
            Return "Error: " & ex.Message & vbCrLf & doc
        End Try

        Return xmlDoc.OuterXml
    End Function

End Class
